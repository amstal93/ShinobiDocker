#!/bin/bash

set -e

source ./fake-environment.env

export CONTAINER_TEST_IMAGE
export DH_REGISTRY_IMAGE
export FAKE_CI

# ./releaseImage.sh [branch1, branch2, ...] [image1, image2, ...] [DEST_IMAGE_NAME] \
#     [APP_FLAVOR] [FORCE_APP_VERSION] [APP_SPECIAL_VERSIONS] [APP_ISDEFAULT] [APP_SPECIAL_TAGS] [APP_SUFFIX] [APP_DISTRO]

echo "==========================================================================="
echo "                          Shinobi images"
echo "==========================================================================="

IMAGES=( ${FAKE_BUILD_IMAGES//,/ } )

for IMAGE in "${IMAGES[@]}"; do
    echo "---------------------------------------------------------------------------"
    echo "      Image ${IMAGE} ..."
    echo "---------------------------------------------------------------------------"
    
    case "${IMAGE}" in
        # "alpine")
        #     # Alpine Linux Node.JS microservice image
        #     IMG_SPECIAL_TAGS="microservice-alpine"
        #     IMG_SUFFIX="microservice-alpine"
        #     ;;

        "phusion")
            # Phusion "NG" image
            IMG_SPECIAL_TAGS="ng"
            IMG_SUFFIX="ng"
            ;;

        *)
            IMG_SPECIAL_TAGS="microservice-$IMAGE"
            IMG_SUFFIX="microservice-$IMAGE"
    esac

    /bin/bash ./gitlab-ci/releaseImage.sh "$SHINOBI_BRANCHES" "$IMAGE" "$DH_REGISTRY_IMAGE" "-" "image" "" "false" "$IMG_SPECIAL_TAGS" "$IMG_SUFFIX" "$IMAGE"
done
